#!/usr/bin/python3
import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

from mixercontroller import MixerController

MixerController().start()

