EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Raspberry Pi HAT"
Date ""
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 5834BC4A
P 5650 6000
F 0 "H1" H 5500 6100 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 5650 5850 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 5550 6000 60  0001 C CNN
F 3 "" H 5550 6000 60  0001 C CNN
	1    5650 6000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5834BCDF
P 6650 6000
F 0 "H2" H 6500 6100 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 6650 5850 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 6550 6000 60  0001 C CNN
F 3 "" H 6550 6000 60  0001 C CNN
	1    6650 6000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5834BD62
P 5650 6550
F 0 "H3" H 5500 6650 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 5650 6400 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 5550 6550 60  0001 C CNN
F 3 "" H 5550 6550 60  0001 C CNN
	1    5650 6550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5834BDED
P 6700 6550
F 0 "H4" H 6550 6650 60  0000 C CNN
F 1 "3mm_Mounting_Hole" H 6700 6400 60  0000 C CNN
F 2 "project_footprints:NPTH_3mm_ID" H 6600 6550 60  0001 C CNN
F 3 "" H 6600 6550 60  0001 C CNN
	1    6700 6550
	1    0    0    -1  
$EndComp
$Comp
L raspberrypi_hat:OX40HAT J3
U 1 1 58DFC771
P 2600 2250
F 0 "J3" H 2950 2350 50  0000 C CNN
F 1 "40HAT" H 2300 2350 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 2600 2450 50  0001 C CNN
F 3 "" H 1900 2250 50  0000 C CNN
	1    2600 2250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J6
U 1 1 58E13683
P 6050 4600
F 0 "J6" H 6050 4750 50  0000 C CNN
F 1 "CONN_02X02" H 6050 4450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 6050 3400 50  0001 C CNN
F 3 "" H 6050 3400 50  0000 C CNN
	1    6050 4600
	1    0    0    -1  
$EndComp
Text Label 5250 4600 0    60   ~ 0
P3V3
Text Label 5250 4700 0    60   ~ 0
P5V
Wire Wire Line
	5250 4600 5750 4600
Wire Wire Line
	5250 4700 5750 4700
Text Label 6950 4600 2    60   ~ 0
P3V3_HAT
Text Label 6950 4700 2    60   ~ 0
P5V_HAT
Wire Wire Line
	6350 4600 6400 4600
Wire Wire Line
	6350 4700 6400 4700
Text Notes 5450 4250 0    60   ~ 0
FLEXIBLE POWER SELECTION
Text Label 7150 2400 2    60   ~ 0
P5V_HAT
Wire Wire Line
	6400 2400 6550 2400
Text Label 5300 2400 0    60   ~ 0
P5V
Wire Wire Line
	5300 2400 5750 2400
Text Notes 5150 1750 0    118  ~ 24
5V Powered HAT Protection
Text Notes 4900 2050 0    60   ~ 0
This is the recommended 5V rail protection for \na HAT with power going to the Pi.\nSee https://github.com/raspberrypi/hats/blob/master/designguide.md#back-powering-the-pi-via-the-j8-gpio-header
$Comp
L raspberrypi_hat:DMG2305UX Q1
U 1 1 58E14EB1
P 6150 2400
F 0 "Q1" V 6300 2550 50  0000 R CNN
F 1 "DMG2305UX" V 6300 2350 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6350 2500 50  0001 C CNN
F 3 "" H 6150 2400 50  0000 C CNN
	1    6150 2400
	0    -1   -1   0   
$EndComp
$Comp
L raspberrypi_hat:DMMT5401 Q2
U 1 1 58E1538B
P 5850 3000
F 0 "Q2" H 6050 3075 50  0000 L CNN
F 1 "DMMT5401" H 6050 3000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 6050 2925 50  0000 L CIN
F 3 "" H 5850 3000 50  0000 L CNN
	1    5850 3000
	-1   0    0    1   
$EndComp
$Comp
L raspberrypi_hat:DMMT5401 Q2
U 2 1 58E153D6
P 6450 3000
F 0 "Q2" H 6650 3075 50  0000 L CNN
F 1 "DMMT5401" H 6650 3000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 6650 2925 50  0000 L CIN
F 3 "" H 6450 3000 50  0000 L CNN
	2    6450 3000
	1    0    0    1   
$EndComp
$Comp
L Device:R R23
U 1 1 58E15896
P 5750 3600
F 0 "R23" V 5830 3600 50  0000 C CNN
F 1 "10K" V 5750 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5680 3600 50  0001 C CNN
F 3 "" H 5750 3600 50  0001 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R24
U 1 1 58E158A1
P 6550 3600
F 0 "R24" V 6630 3600 50  0000 C CNN
F 1 "47K" V 6550 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6480 3600 50  0001 C CNN
F 3 "" H 6550 3600 50  0001 C CNN
	1    6550 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 58E15A41
P 5750 3800
F 0 "#PWR01" H 5750 3550 50  0001 C CNN
F 1 "GND" H 5750 3650 50  0000 C CNN
F 2 "" H 5750 3800 50  0000 C CNN
F 3 "" H 5750 3800 50  0000 C CNN
	1    5750 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 58E15A9E
P 6550 3800
F 0 "#PWR02" H 6550 3550 50  0001 C CNN
F 1 "GND" H 6550 3650 50  0000 C CNN
F 2 "" H 6550 3800 50  0000 C CNN
F 3 "" H 6550 3800 50  0000 C CNN
	1    6550 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3800 5750 3750
Wire Wire Line
	6550 3800 6550 3750
Wire Wire Line
	6550 3200 6550 3300
Wire Wire Line
	6150 2650 6150 3300
Wire Wire Line
	6150 3300 6550 3300
Connection ~ 6550 3300
Wire Wire Line
	5750 3200 5750 3350
Wire Wire Line
	6050 3000 6050 3350
Wire Wire Line
	5750 3350 6050 3350
Connection ~ 5750 3350
Wire Wire Line
	6250 3350 6250 3000
Connection ~ 6050 3350
Wire Wire Line
	5750 2800 5750 2400
Connection ~ 5750 2400
Wire Wire Line
	6550 2800 6550 2400
Connection ~ 6550 2400
$Comp
L Device:R R7
U 1 1 58E22085
P 6100 4300
F 0 "R7" V 6180 4300 50  0000 C CNN
F 1 "DNP" V 6100 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6030 4300 50  0001 C CNN
F 3 "" H 6100 4300 50  0001 C CNN
	1    6100 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 4700 5750 4950
Wire Wire Line
	5750 4950 5950 4950
Connection ~ 5750 4700
Wire Wire Line
	6250 4950 6400 4950
Wire Wire Line
	6400 4950 6400 4700
Connection ~ 6400 4700
Wire Wire Line
	6400 4600 6400 4300
Wire Wire Line
	6400 4300 6250 4300
Connection ~ 6400 4600
Wire Wire Line
	5950 4300 5750 4300
Wire Wire Line
	5750 4300 5750 4600
Connection ~ 5750 4600
Text Notes 5300 5700 0    118  ~ 24
Mounting Holes
Text Notes 1650 2000 0    118  ~ 24
40-Pin HAT Connector
Text Label 800  4150 0    60   ~ 0
GND
Wire Wire Line
	2000 4150 800  4150
Text Label 800  3450 0    60   ~ 0
GND
Wire Wire Line
	2000 3450 800  3450
Text Label 800  2650 0    60   ~ 0
GND
Wire Wire Line
	2000 2650 800  2650
Text Label 800  2250 0    60   ~ 0
P3V3_HAT
Wire Wire Line
	2000 2250 800  2250
Wire Wire Line
	3200 2850 4400 2850
Wire Wire Line
	3200 3150 4400 3150
Wire Wire Line
	3200 3650 4400 3650
Wire Wire Line
	3200 3850 4400 3850
Text Label 4400 2850 2    60   ~ 0
GND
Text Label 4400 3150 2    60   ~ 0
GND
Text Label 4400 3650 2    60   ~ 0
GND
Text Label 4400 3850 2    60   ~ 0
GND
Text Label 4400 2450 2    60   ~ 0
GND
Wire Wire Line
	3200 2450 4400 2450
Text Label 4400 2350 2    60   ~ 0
P5V_HAT
Wire Wire Line
	3200 2350 4400 2350
Text Label 4400 2250 2    60   ~ 0
P5V_HAT
Wire Wire Line
	3200 2250 4400 2250
Text Notes 7150 4950 0    60   ~ 0
HAT spec indicates to NEVER\npower the 3.3V pins on the Raspberry Pi \nfrom the HAT header. Only connect the 3.3V\npower from the Pi if the HAT does not have\n3.3V on board.\n\nIF you are designing a board that could\neither be powered by the Pi or from the HAT\nthe jumpers here can be used.\n\nIn most cases, either design the HAT \nto provide the 5V to the Pi and use the\nprotection circuit above OR power the\nHAT from the Pi and directly connect\nthe P3V3 and P5V to the P3V3_HAT and P5V_HAT\npins.
Text Notes 850  1250 0    100  ~ 0
This is based on the official Raspberry Pi spec to be able to call an extension board a HAT.\nhttps://github.com/raspberrypi/hats/blob/master/designguide.md
Wire Wire Line
	6550 3300 6550 3450
Wire Wire Line
	5750 3350 5750 3450
Wire Wire Line
	6050 3350 6250 3350
Wire Wire Line
	5750 2400 5900 2400
Wire Wire Line
	6550 2400 7150 2400
Wire Wire Line
	5750 4700 5850 4700
Wire Wire Line
	6400 4700 6950 4700
Wire Wire Line
	6400 4600 6950 4600
Wire Wire Line
	5750 4600 5850 4600
$Comp
L Interface_UART:MAX202 U1
U 1 1 614D22EE
P 13225 3125
F 0 "U1" H 13225 4506 50  0000 C CNN
F 1 "MAX202" H 13225 4415 50  0000 C CNN
F 2 "Package_DIP:DIP-18_W7.62mm" H 13275 2075 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max202.pdf" H 13225 3225 50  0001 C CNN
	1    13225 3125
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C1
U 1 1 614D493E
P 11975 2050
F 0 "C1" H 12090 2096 50  0000 L CNN
F 1 "CP1" H 12090 2005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 11975 2050 50  0001 C CNN
F 3 "~" H 11975 2050 50  0001 C CNN
	1    11975 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C2
U 1 1 614DAA1F
P 12425 2375
F 0 "C2" H 12540 2421 50  0000 L CNN
F 1 "CP1" H 12540 2330 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 12425 2375 50  0001 C CNN
F 3 "~" H 12425 2375 50  0001 C CNN
	1    12425 2375
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C4
U 1 1 614DD19D
P 14025 2375
F 0 "C4" H 14140 2421 50  0000 L CNN
F 1 "CP1" H 14140 2330 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 14025 2375 50  0001 C CNN
F 3 "~" H 14025 2375 50  0001 C CNN
	1    14025 2375
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C5
U 1 1 614E00B1
P 14175 3025
F 0 "C5" H 14290 3071 50  0000 L CNN
F 1 "CP1" H 14290 2980 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 14175 3025 50  0001 C CNN
F 3 "~" H 14175 3025 50  0001 C CNN
	1    14175 3025
	0    1    1    0   
$EndComp
$Comp
L Device:CP1 C3
U 1 1 614E2DC5
P 13600 1850
F 0 "C3" H 13715 1896 50  0000 L CNN
F 1 "CP1" H 13715 1805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 13600 1850 50  0001 C CNN
F 3 "~" H 13600 1850 50  0001 C CNN
	1    13600 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	13225 1900 13225 1925
Wire Wire Line
	13450 1850 13225 1850
Wire Wire Line
	13225 1850 13225 1900
Connection ~ 13225 1900
Wire Wire Line
	11975 1900 13225 1900
Wire Wire Line
	14025 2725 14350 2725
Wire Wire Line
	14350 2725 14350 1850
Wire Wire Line
	14350 1850 13750 1850
Text Label 11300 2200 0    60   ~ 0
GND
Wire Wire Line
	11975 2200 11300 2200
Text Label 14325 3025 0    60   ~ 0
GND
Text Label 13325 4325 0    60   ~ 0
GND
Wire Wire Line
	13225 4325 13325 4325
Wire Wire Line
	11975 1900 11525 1900
Wire Wire Line
	11525 1900 11525 1650
Connection ~ 11975 1900
Text Label 11525 1650 2    60   ~ 0
P5V_HAT
Wire Wire Line
	3200 2550 4400 2550
Wire Wire Line
	3200 2650 4400 2650
Text Label 4400 2550 0    50   ~ 0
UART_TX
Text Label 4400 2650 0    50   ~ 0
UART_RX
Text Label 14025 3425 0    50   ~ 0
UART_RX
Text Label 14025 3825 0    50   ~ 0
UART_TX
Text Label 12425 3425 2    50   ~ 0
SERIAL_RX
Text Label 12425 3825 2    50   ~ 0
SERIAL_TX
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 61522EFB
P 10850 3575
F 0 "J1" H 10822 3507 50  0000 R CNN
F 1 "Conn_01x03_Male" H 10822 3598 50  0000 R CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 10850 3575 50  0001 C CNN
F 3 "~" H 10850 3575 50  0001 C CNN
	1    10850 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	11175 3675 11175 4325
Wire Wire Line
	11175 4325 13225 4325
Connection ~ 13225 4325
Wire Wire Line
	11600 3575 11600 3825
Wire Wire Line
	11600 3825 12425 3825
Wire Wire Line
	11175 3475 12425 3475
Wire Wire Line
	12425 3475 12425 3425
$Comp
L Device:R R9
U 1 1 58E2218F
P 6100 4950
F 0 "R9" V 6180 4950 50  0000 C CNN
F 1 "DNP" V 6100 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6030 4950 50  0001 C CNN
F 3 "" H 6100 4950 50  0001 C CNN
	1    6100 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	11050 3575 11600 3575
Wire Wire Line
	11050 3475 11125 3475
Wire Wire Line
	11125 3475 11125 3675
Wire Wire Line
	11125 3675 11175 3675
Wire Wire Line
	11175 3475 11175 3625
Wire Wire Line
	11175 3625 11050 3625
Wire Wire Line
	11050 3625 11050 3675
$Comp
L Isolator:PC817 U?
U 1 1 61562FDC
P 11550 5725
F 0 "U?" H 11550 6050 50  0000 C CNN
F 1 "PC817" H 11550 5959 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 11350 5525 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 11550 5725 50  0001 L CNN
	1    11550 5725
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61564475
P 11100 5625
F 0 "R?" H 11170 5671 50  0000 L CNN
F 1 "200" H 11170 5580 50  0000 L CNN
F 2 "" V 11030 5625 50  0001 C CNN
F 3 "~" H 11100 5625 50  0001 C CNN
	1    11100 5625
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:S8050 Q?
U 1 1 61564B92
P 12350 5825
F 0 "Q?" H 12540 5871 50  0000 L CNN
F 1 "S8050" H 12540 5780 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 12550 5750 50  0001 L CIN
F 3 "http://www.unisonic.com.tw/datasheet/S8050.pdf" H 12350 5825 50  0001 L CNN
	1    12350 5825
	1    0    0    -1  
$EndComp
$Comp
L Device:D D?
U 1 1 61565741
P 12450 5475
F 0 "D?" V 12496 5396 50  0000 R CNN
F 1 "1N4148" V 12405 5396 50  0000 R CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 12450 5475 50  0001 C CNN
F 3 "~" H 12450 5475 50  0001 C CNN
	1    12450 5475
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 6156B476
P 12000 5825
F 0 "R?" V 12207 5825 50  0000 C CNN
F 1 "1k" V 12116 5825 50  0000 C CNN
F 2 "" V 11930 5825 50  0001 C CNN
F 3 "~" H 12000 5825 50  0001 C CNN
	1    12000 5825
	0    1    1    0   
$EndComp
Wire Wire Line
	11850 5625 12175 5625
Wire Wire Line
	12175 5625 12175 5325
Wire Wire Line
	12175 5325 12450 5325
Connection ~ 12450 5325
Wire Wire Line
	12450 5600 13025 5600
Wire Wire Line
	10950 5625 10525 5625
Wire Wire Line
	11250 5825 10525 5825
Text Label 10525 5825 2    60   ~ 0
GND
Text Label 12450 6100 2    60   ~ 0
GND
Wire Wire Line
	12450 6025 12450 6100
Wire Wire Line
	12450 5325 13025 5325
Wire Wire Line
	12450 5325 12450 5200
Text Label 12450 5200 1    60   ~ 0
P5V_HAT
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 615AD3D1
P 14200 5525
F 0 "J?" H 14172 5457 50  0000 R CNN
F 1 "Conn_01x03_Male" H 14172 5548 50  0000 R CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 14200 5525 50  0001 C CNN
F 3 "~" H 14200 5525 50  0001 C CNN
	1    14200 5525
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 615B20B9
P 14225 4800
F 0 "J?" H 14197 4682 50  0000 R CNN
F 1 "Conn_01x03_Male" H 14197 4773 50  0000 R CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 14225 4800 50  0001 C CNN
F 3 "~" H 14225 4800 50  0001 C CNN
	1    14225 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 3350 775  3350
Text Label 775  3350 2    50   ~ 0
SPI_CLK
Wire Wire Line
	2000 3150 775  3150
Text Label 775  3150 2    50   ~ 0
SPI_MOSI
Wire Wire Line
	3200 3350 4400 3350
Text Label 4400 3350 2    50   ~ 0
SPI_CE0
Text Label 14000 5525 2    50   ~ 0
SPI_MOSI
Text Label 14025 4700 2    50   ~ 0
P5V_HAT
$Comp
L power:GND #PWR?
U 1 1 615E500C
P 14025 4800
F 0 "#PWR?" H 14025 4550 50  0001 C CNN
F 1 "GND" H 14025 4650 50  0000 C CNN
F 2 "" H 14025 4800 50  0000 C CNN
F 3 "" H 14025 4800 50  0000 C CNN
	1    14025 4800
	1    0    0    -1  
$EndComp
Text Label 14000 5425 2    50   ~ 0
SPI_CLK
Text Label 14000 5625 2    50   ~ 0
SPI_CE0
$EndSCHEMATC
