import sys
import os.path
import threading

from singletone import SingletonMeta
import subprocess
from logging_handler import Log
import time


class Updater(metaclass=SingletonMeta):
    def __init__(self, parent=None):
        self.mc = parent
        self.filename = ".updates_"

    def create_upd_file(self):
        Log().log.info("Update available file created")
        f = open(self.filename, "a")
        f.close()

    def delete_upd_file(self):
        os.remove(self.filename)

    def updates_available(self):
        return os.path.isfile(self.filename)

    def check_updates(self):
        _thread = threading.Thread(target=self.check_updates_thread, args=())
        _thread.start()

    def check_updates_thread(self):
        Log().log.info("Update check start")
        subprocess.Popen(["git", "remote", "update"])
        process = subprocess.Popen(["git", "status"], stdout=subprocess.PIPE)
        process.wait(5)
        output = process.communicate()[0]
        if "Your branch is behind" in str(output):
            self.create_upd_file()
            self.mc.update_available = True
        else:
            Log().log.info("No updates available")
            self.mc.update_available = False

    def pull_updates(self):
        Log().log.info("Pull updates process")
        subprocess.Popen(["git", "reset", "--hard", "HEAD~1"])
        subprocess.Popen(["git", "pull", "--rebase"])
        process = subprocess.Popen(["git", "status"], stdout=subprocess.PIPE)
        output = process.communicate()[0]
        Log().log.debug(output)
        if "is up to date" in str(output):
            self.delete_upd_file()
            return True
        else:
            return False

    def restart(self):
        Log().log.info("Try to restart process")
        subprocess.run('/bin/bash rerun.sh', shell=True)
        #os.execv("/bin/bash", ["rerun.sh"])
        exit()

    def start_update_process(self):
        _thread = threading.Thread(target=self.update_thread, args=())
        _thread.start()

    def update_thread(self):
        Log().log.info("Start update triggered")
        self.mc.ui.show_warning("Оновлюється", False)
        time.sleep(1)
        if Updater().pull_updates():
            Log().log.info("Update successful")
            self.mc.ui.show_warning("Оновлення успішне.\n Програма перезапуститься", False)
            time.sleep(4)
            Updater().restart()
        else:
            Log().log.info("Update unsuccessful")
            self.mc.ui.show_warning("Оновлення не успішне.")
