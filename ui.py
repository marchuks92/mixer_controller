import sys
import time
import threading

from PyQt5 import QtWidgets, uic, QtGui, QtCore
from PyQt5.QtGui import QPalette, QColor
from signalslot import Signal
from config_mgr import ConfigMgr
from logging_handler import Log


class MainWindow(QtWidgets.QMainWindow):
    showMessageBox = QtCore.pyqtSignal(str, str, str, str)

    def __init__(self, parent, is_black, is_full_screen):
        super(MainWindow, self).__init__()
        self._parent = parent
        self.mc = parent.mc
        if is_black:
            self._ui = uic.loadUi('mainwindow_black.ui', self)
        else:
            self._ui = uic.loadUi('mainwindow.ui', self)
        self._connect_buttons_to_slots()
        if is_full_screen:
            self.showFullScreen()
        else:
            self.show()
        self._is_tracking_enabled = False
        self._ui.closeEvent = self._close_event
        self._tracking_value = 0
        self._menu_visible_counter = 0
        self.can_show_warn_box = True

    def _connect_buttons_to_slots(self):
        self.start_tracking_btn.clicked.connect(self._start_tracking)
        self.stop_tracking_btn.clicked.connect(self._stop_tracking)
        self.num_1_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "1"))
        self.num_0_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "0"))
        self.num_2_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "2"))
        self.num_3_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "3"))
        self.num_4_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "4"))
        self.num_5_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "5"))
        self.num_6_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "6"))
        self.num_7_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "7"))
        self.num_8_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "8"))
        self.num_9_btn.clicked.connect(lambda: self.input_box.setText(self.input_box.text() + "9"))
        self.num_clear_btn.clicked.connect(lambda: self.input_box.setText(""))
        self.num_enter_btn.clicked.connect(self._enter_pressed)
        self.actionExit.triggered.connect(self._close_event)
        self.actionCheck_for_updates.triggered.connect(self._check_for_updates)
        self.stop_tracking_btn.setEnabled(False)
        self.clear_history_btn.clicked.connect(self._clear_history)

        self._previous_weights_model = QtGui.QStandardItemModel()
        self.previous_weights.setModel(self._previous_weights_model)
        self.warning_frame.hide()
        self.warn_ok_btn.clicked.connect(self.warn_ok_clicked)

        self.connect_weigh_alert_btns()

        self.menubar.hide()
        self.label_2.mousePressEvent = self.show_menubar

        if self.mc.updater.updates_available():
            self.update_btn.show()
        else:
            self.update_btn.hide()
        self.update_btn.clicked.connect(self.start_update)

    def connect_weigh_alert_btns(self):
        self.weight_before_0_btn.clicked.connect(self.weight_before_0_btn_clicked)
        self.weight_before_0_btn.setText(str(ConfigMgr().get_tracking_cfg("alert_btn_0")))
        self.weight_before_1_btn.clicked.connect(self.weight_before_1_btn_clicked)
        self.weight_before_1_btn.setText(str(ConfigMgr().get_tracking_cfg("alert_btn_1")))
        self.weight_before_2_btn.clicked.connect(self.weight_before_2_btn_clicked)
        self.weight_before_2_btn.setText(str(ConfigMgr().get_tracking_cfg("alert_btn_2")))
        self.weight_before_3_btn.clicked.connect(self.weight_before_3_btn_clicked)
        self.weight_before_3_btn.setText(str(ConfigMgr().get_tracking_cfg("alert_btn_3")))
        self.weight_before_4_btn.clicked.connect(self.weight_before_4_btn_clicked)
        self.weight_before_4_btn.setText(str(ConfigMgr().get_tracking_cfg("alert_btn_4")))

        sel_idx = ConfigMgr().get_tracking_cfg("alert_before_idx")
        if sel_idx == 0:
            self.weight_before_0_btn_clicked()
        elif sel_idx == 1:
            self.weight_before_1_btn_clicked()
        elif sel_idx == 2:
            self.weight_before_2_btn_clicked()
        elif sel_idx == 3:
            self.weight_before_3_btn_clicked()
        elif sel_idx == 4:
            self.weight_before_4_btn_clicked()

    def weight_before_0_btn_clicked(self):
        self.mc.tracker.update_alert_before_weight(ConfigMgr().get_tracking_cfg("alert_btn_0"))
        self.make_btn_toggled(self.weight_before_0_btn)
        ConfigMgr().change_alert_before_idx(0)

    def weight_before_1_btn_clicked(self):
        self.mc.tracker.update_alert_before_weight(ConfigMgr().get_tracking_cfg("alert_btn_1"))
        self.make_btn_toggled(self.weight_before_1_btn)
        ConfigMgr().change_alert_before_idx(1)

    def weight_before_2_btn_clicked(self):
        self.mc.tracker.update_alert_before_weight(ConfigMgr().get_tracking_cfg("alert_btn_2"))
        self.make_btn_toggled(self.weight_before_2_btn)
        ConfigMgr().change_alert_before_idx(2)

    def weight_before_3_btn_clicked(self):
        self.mc.tracker.update_alert_before_weight(ConfigMgr().get_tracking_cfg("alert_btn_3"))
        self.make_btn_toggled(self.weight_before_3_btn)
        ConfigMgr().change_alert_before_idx(3)

    def weight_before_4_btn_clicked(self):
        self.mc.tracker.update_alert_before_weight(ConfigMgr().get_tracking_cfg("alert_btn_4"))
        self.make_btn_toggled(self.weight_before_4_btn)
        ConfigMgr().change_alert_before_idx(4)

    def make_btn_toggled(self, btn):
        color = self.start_tracking_btn.palette().button().color()
        self.weight_before_0_btn.setStyleSheet("background-color:" + color.name() + ";")
        self.weight_before_1_btn.setStyleSheet("background-color:" + color.name() + ";")
        self.weight_before_2_btn.setStyleSheet("background-color:" + color.name() + ";")
        self.weight_before_3_btn.setStyleSheet("background-color:" + color.name() + ";")
        self.weight_before_4_btn.setStyleSheet("background-color:" + color.name() + ";")
        btn.setStyleSheet("background-color:yellow; border: none;")

    def update_TrackingPogressBar(self, percent):
        self.trackingPogressBar.setValue(percent)

    def show_menubar(self,  event):
        Log().log.info("MenuBar is visible")
        self._menu_visible_counter += 1
        if self._menu_visible_counter == 5:
            self.menubar.show()

    def _clear_history(self):
        self._previous_weights_model.clear()
        self.mc.tracker.clear_history()

    def add_weight_to_history(self, val):
        item = QtGui.QStandardItem(str(val))
        self._previous_weights_model.appendRow(item)

    def _enter_pressed(self):
        pass

    def _close_event(self, event):
        Log().log.info("MainWindow: got exit event")
        self._parent.close_event(event)

    def get_expected_tracking_value(self):
        if len(self.input_box.text()) > 0:
            return int(self.input_box.text())
        else:
            return 0

    def _start_tracking(self):
        if self.get_expected_tracking_value() == 0:
            return
        self.start_tracking_btn.setEnabled(False)
        self.stop_tracking_btn.setEnabled(True)
        self._is_tracking_enabled = True
        self.numpad_widget.setEnabled(False)
        self.mc.tracker.wait_for_value(self.get_expected_tracking_value())

    def _stop_tracking(self):
        self.start_tracking_btn.setEnabled(True)
        self.stop_tracking_btn.setEnabled(False)
        self.numpad_widget.setEnabled(True)
        self._is_tracking_enabled = False
        self.mc.tracker.stop_waiting()

    def _tracking_reached_int(self, real_val):
        self.add_weight_to_history(real_val)
        self._stop_tracking()
        self._parent.show_warning("УВАГА\nПотрібна маса досягнута")

    def tracking_reached(self, real_val):
        self._tracking_reached_int(real_val)
        self.mc.buzzer.beep_start()

    def update_current_weight(self, value):
        self.summary_weight_lbl.setText(str(value))
        self.current_weight_lbl.setText(str(value - self.mc.tracker.history_sum))

    def show_warn_box(self, req):
        if self.can_show_warn_box is True:
            self.warn_lbl.setText(req.message)
            if req.need_ok_btn:
                self.warn_ok_btn.show()
                self.can_show_warn_box = False
            else:
                self.warn_ok_btn.hide()
                self.can_show_warn_box = True

            self.warning_frame.show()
            return True
        else:
            return False

    def warn_ok_clicked(self):
        self.warning_frame.hide()
        self.can_show_warn_box = True
        self.mc.buzzer.beep_stop()

    def _check_for_updates(self):
        self.mc.updater.check_updates()

    def show_update_btn(self):
        self.update_btn.show()

    def start_update(self):
        self.mc.updater.start_update_process()


class WarnMsg:
    def __init__(self, message, need_ok_btn=True):
        self.message = message
        self.need_ok_btn = need_ok_btn


class Ui(QtCore.QObject):
    def __init__(self, parent):
        super(Ui, self).__init__()
        self._app = None
        self._window = None
        self._thread = None
        self._running = True
        self._black_theme = ConfigMgr().get_ui_cfg("black_theme")
        self._full_screen = False
        self.on_exit = Signal()
        self.warnings = []
        self.mc = parent
        self.update_available = False

    def run_ui(self):
        self._thread = threading.Thread(target=self._thread_ui, args=())
        self._thread.start()

    def _thread_ui(self):
        self._app = QtWidgets.QApplication(sys.argv)
        screen_resolution = self._app.desktop().screenGeometry()
        if screen_resolution.width() <= 800:
            self._full_screen = True
        self._window = MainWindow(self, self._black_theme,  self._full_screen)
        self._app.exec_()

    def update_weight(self, weight, **kwargs):
        if self._window:
            self._window.update_current_weight(weight)

    def update_progress(self, val):
        self._window.update_TrackingPogressBar(val)

    def close_event(self, event):
        Log().log.info("UI: got exit event")
        self.on_exit.emit()
        self._app.quit()
        self._running = False

    def tracking_reached(self, expected_val, real_val, **kwargs):
        self._window.tracking_reached(real_val)

    def show_warning(self, message, need_ok_btn=True):
        self.warnings.append(WarnMsg(message, need_ok_btn))

    def loop(self):
        while self._running:
            if self._window:
                if len(self.warnings) > 0:
                    if self._window.show_warn_box(self.warnings[0]):
                        self.warnings.pop(0)
                if self.update_available:
                    self._window.show_update_btn()
            time.sleep(0.1)

    def no_connection_msg(self, **kwargs):
        self.show_warning("Немає зв'язку з вагою")
