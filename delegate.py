class Delegate:
    def __init__(self):
        self._callbacks_list = []

    def subscribe(self, callback):
        self._callbacks_list.append(callback)

    def notify_all(self, args):
        for subscriber in self._callbacks_list:
            # try:
                subscriber(args)
            # except:
            #     pass

    def unsubscribe(self, callback):
        self._callbacks_list.remove(callback)
