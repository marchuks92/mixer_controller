import RPi.GPIO as GPIO
from singletone import SingletonMeta


class Buzzer(metaclass=SingletonMeta):

    def __init__(self):
        # Broadcom pin 18 (P1 pin 12)
        self.buzzer_pin = 18
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.buzzer_pin, GPIO.OUT)

    def beep_start(self):
        GPIO.output(self.buzzer_pin, GPIO.HIGH)

    def beep_stop(self):
        GPIO.output(self.buzzer_pin, GPIO.LOW)
