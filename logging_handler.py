import logging
import sys
from singletone import SingletonMeta


class Log(metaclass=SingletonMeta):
    def __init__(self, *args, **kwargs):
        self._root = logging.getLogger()
        self._root.setLevel(logging.DEBUG)

        self._handler = logging.StreamHandler(sys.stdout)
        self._handler.setLevel(logging.DEBUG)

        self._formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self._handler.setFormatter(self._formatter)
        self._root.addHandler(self._handler)
        # self.log = logging.getLogger(self.__class__.__name__)
        self.log = self._root

    def set_level(self, name):
        if str(name).lower() == "debug":
            self._handler.setLevel(logging.DEBUG)
        elif str(name).lower() == "info":
            self._handler.setLevel(logging.INFO)
        elif str(name).lower() == "warning":
            self._handler.setLevel(logging.WARNING)
        elif str(name).lower() == "error":
            self._handler.setLevel(logging.ERROR)
