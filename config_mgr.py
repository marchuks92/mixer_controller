import yaml
from yaml.loader import SafeLoader
from singletone import SingletonMeta
from logging_handler import Log
import fileinput, re


class ConfigMgr(metaclass=SingletonMeta):

    def __init__(self, cfg_file=""):
        self.cfg_file = cfg_file
        self.config_data = None

        if self.config_data is None:
            Log().log.info("start parsing config file '%s'" % cfg_file)
            with open(cfg_file) as f:
                self.config_data = yaml.load(f, Loader=SafeLoader)
                Log().log.info("Parsed config " + str(self.config_data))

    def get_ui_cfg(self, name):
        return self.config_data["ui"][name]

    def get_serial_cfg(self, name):
        return self.config_data["serial"][name]

    def get_system_cfg(self, name):
        return self.config_data["system"][name]

    def get_tracking_cfg(self, name):
        return self.config_data["tracking"][name]

    def change_alert_before_idx(self, idx):
        for line in fileinput.input(self.cfg_file, inplace=True):
            # inside this loop the STDOUT will be redirected to the file
            # the comma after each print statement is needed to avoid double line breaks
            print(re.sub(r"alert_before_idx: \d", "alert_before_idx: " + str(idx), line), end="")
