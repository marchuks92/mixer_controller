import threading
import time
from signalslot import Signal
from singletone import SingletonMeta
import serial
from config_mgr import ConfigMgr
from logging_handler import Log


class WeightIndicator(metaclass=SingletonMeta):
    def __init__(self, parent):
        self._emulate = ConfigMgr().get_serial_cfg("emulate")
        self._port = ConfigMgr().get_serial_cfg("port")
        self._baudrate = ConfigMgr().get_serial_cfg("baudrate")
        self.weight_changed = Signal(args=['weight'])
        self.no_connection = Signal()
        self._current_weight = 0
        self._thread = None
        self._thread_running = False
        self._serial_port = None
        self.mc = parent
        Log().log.debug("Weighing controller at: port=%s baudrate=%s" % (str(self._port), str(self._baudrate)))

    def set_port(self, port, baudrate):
        self._port = port
        self._baudrate = baudrate

    def get_weight(self):
        return self._current_weight

    def connect(self):
        if self._emulate:
            Log().log.info("Starting weighting emulation")
            self._thread = threading.Thread(target=self.emulate_thread_function, args=())
            self._thread_running = True
            self._thread.start()
        else:
            Log().log.info("Connect to weighting indicator")
            try:
                self._serial_port = serial.Serial(port=self._port, baudrate=self._baudrate, bytesize=8, timeout=2,
                                                  stopbits=serial.STOPBITS_ONE)
                self._thread = threading.Thread(target=self.thread_function, args=())
                self._thread_running = True
                self._thread.start()
            except serial.SerialException:
                Log().log.error("Cant connect to device " + str(self._port))
                self.no_connection.emit()

    def thread_function(self):
        while self._thread_running:
            if self._serial_port.in_waiting > 0:
                self._serial_port.flush()
                serial_string = self._serial_port.readline()
                # print(serial_string)
                decoded_weight = self.decode_weight(serial_string)
                self.weight_changed.emit(weight=decoded_weight)
            time.sleep(0.1)

    def emulate_thread_function(self):
        x = 0
        while self._thread_running:
            self._current_weight = x
            self.weight_changed.emit(weight=x)
            x += 5
            time.sleep(1)

    def on_exit_event(self, **kwargs):
        Log().log.info("WeightIndicator: got exit event")
        self._thread_running = False
        if self._thread:
            self._thread.join()

    def decode_weight(self, in_buffer):
        # 0A 77 6E 30 30 30 35 39 39 20 6B 67 0D  .wn000599 kg.
        # 0A 77 6E 2D 32 34 38 30 38 20 6B 67 0D  .wn-24808 kg.
        # 0A 00 00 00 00 00 00 00 00 00 4F 4C 0D  ..........OL.
        # 0A 77 6E 30 30 30 30 30 30 20 6B 67 0D  .wn000000 kg.
        #                                         .wn000.000kg.
        in_buffer = in_buffer.strip()
        if in_buffer[:2] != b'wn':
            return 0
        if in_buffer[-2:] == b'OL':
            return 0

        val = in_buffer[2:-2].decode('utf-8')
        if '.' in val:
            return float(val)
        else:
            return int(val)
