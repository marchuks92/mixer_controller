from signalslot import Signal
from singletone import SingletonMeta
from logging_handler import Log


class WeightTracker(metaclass=SingletonMeta):
    def __init__(self, parent=None):
        self._weight_history = []
        self.expected_value = 0
        self._is_process_started = False
        self.history_sum = 0
        self.value_matched = Signal(args=['expected_val', 'real_val'])
        self.mc = parent
        self.alert_before_weight = 0

    def wait_for_value(self, val):
        Log().log.debug("Wait for value: " + str(val))
        self.expected_value = val
        self._is_process_started = True

    def stop_waiting(self):
        Log().log.debug("Stop waiting for value")
        self.expected_value = 0
        self._is_process_started = False

    def tracking_reached(self, weight):
        current_weight_part = weight - self.history_sum
        Log().log.debug("Tracking reached: sum=%s cur=%s" % (str(weight), str(current_weight_part)))
        self._weight_history.append(current_weight_part)
        self.history_sum += current_weight_part
        self.value_matched.emit(real_val=current_weight_part, expected_val=self.expected_value)

    def update_current_value(self, weight, **kwargs):
        if self._is_process_started is True:
            if self.expected_value <= weight + self.alert_before_weight - self.history_sum:
                self.tracking_reached(weight)
            if self.expected_value > 0:
                self.mc.ui.update_progress(((weight - self.history_sum) * 100) / self.expected_value)

    def clear_history(self):
        self._weight_history.clear()
        self.history_sum = 0

    def update_alert_before_weight(self, val):
        self.alert_before_weight = val
