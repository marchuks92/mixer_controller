from ui import Ui
from weight_connector import WeightIndicator
from tracker import WeightTracker
from buzzer import Buzzer
from config_mgr import ConfigMgr
from logging_handler import Log
from updater import Updater
from singletone import SingletonMeta


class MixerController(metaclass=SingletonMeta):

    def __init__(self):
        Log().log.info("Mixer controller started")
        self.config = ConfigMgr('config.yaml')
        Log().set_level(ConfigMgr().get_system_cfg("log_level"))
        self.updater = Updater(self)
        if ConfigMgr().get_system_cfg("check_updates_on_start"):
            self.updater.check_updates()
        self.ui = Ui(self)
        self.indicator = WeightIndicator(self)
        self.tracker = WeightTracker(self)
        self.buzzer = Buzzer()

        self.indicator.weight_changed.connect(self.ui.update_weight)
        self.indicator.weight_changed.connect(self.tracker.update_current_value)
        self.indicator.no_connection.connect(self.ui.no_connection_msg)

        self.ui.on_exit.connect(self.indicator.on_exit_event)
        self.ui.on_exit.connect(self.on_exit)

        self.tracker.value_matched.connect(self.ui.tracking_reached)
        idx = ConfigMgr().get_tracking_cfg("alert_before_idx")
        self.tracker.update_alert_before_weight(ConfigMgr().get_tracking_cfg("alert_btn_" + str(idx)))

    def start(self):
        self.indicator.connect()
        self.ui.run_ui()
        self.ui.loop()

    def on_exit(self, **kwargs):
        Log().log.info("MixerController: got exit event")
