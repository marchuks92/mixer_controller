#!/bin/bash
ps aux | grep main.py | grep -v grep | awk '{ print $2 }' | xargs kill -9
python3 /home/pi/mixer_controller/main.py &
